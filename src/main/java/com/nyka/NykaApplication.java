package com.nyka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NykaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NykaApplication.class, args);
	}

}
